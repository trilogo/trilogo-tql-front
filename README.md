# Trilogo - Frontend Developer Challenge
## Trilogo Query Language (TQL)

## Requirements

Create a web application responsible for converting UI elements (filters) into TQL queries. Just that.

- design a UI just like the images provided
- convert UI elements (filters) into TQL string after pressing the search button
- store all state (filters settings) on Redux (don't use component state. Just Redux state.)
- call a fictional API endpoint at https://api.trilogo.com.br/tickets?tql=TQL_STRING
- all functional tests should pass (additional tests may be implemented as necessary)

## Trilogo Query Language

A query has 4 parts: fields, operators, values and keywords.

- Field – Fields are different types of information in the system. Trilogo fields include id, description, author, etc.
- Operator – Operators are the heart of the query. They relate the field to the value. Common operators include equals (=), not equals (!=), less than (<), etc.
- Value – Values are the actual data in the query. They are usually the item for which we are looking.
- Keyword – Keywords are specific words in the language that have special meaning. In this test we will be focused on AND only.

### Fields

A field in TQL is a word that represents a Trilogo field. In a clause, a field is followed by an operator, which in turn is followed by one or more values. The operator compares the value of the field with one or more values on the right, such that only true results are retrieved by the clause.

#### List of fields (for the matter of this test)

- project
- type
- status
- assignee
- term

### Operators

An operator in TQL is one or more symbols or words which compares the value of a field on its left with one or more values on its right, such that only true results are retrieved by the clause.

#### List of operators (for the matter of this test)

- IN
- CONTAINS: ~

### Keywords

A keyword in TQL is a word or phrase that does (or is) any of the following:

- joins two or more clauses together to form a complex TQL query
- alters the logic of one or more clauses
- alters the logic of operators
- has an explicit definition in a TQL query
- performs a specific function that alters the results of a TQL query.

#### List of keywords (for the matter of this test)

- AND

## TQL samples

[sample image](http://ugolabs.com/tql3.png)
	
	GET /tickets?tql=status IN (In Progress, Open, Reopened)

[sample image](http://ugolabs.com/tql4.png)

	GET /tickets?tql=type IN (Bug) AND status IN (In Progress, Open, Reopened) AND term ~ "urgent"

[sample image](http://ugolabs.com/tql1.png)
	
	GET /tickets?tql=type IN (Bug) AND status IN (In Progress, Open, Reopened)


### Non-functional

- All code should be written in JavaScript using React and Redux
- Challenge is submitted as pull request against this repo ([fork it](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html) and [create a pull request](https://confluence.atlassian.com/bitbucket/create-a-pull-request-to-merge-your-change-774243413.html))
- Documentation and maintainability is a plus

## Getting Started

Begin by forking this repo and cloning your fork.

## Important tip!

Something is better than nothing, even if it is less than we wanted.